const express = require('express')

const bookController = require('../controllers/book')

const router = express.Router();

router.get('/contacts', bookController.contacts)


module.exports = router