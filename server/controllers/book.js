const getContacts = require('../utils/contacts')

exports.contacts = async (req, res, next) => {
    try {
        await getContacts((error, data) => {
            if (error) {
                const error = new Error(error)
                error.statusCode = 500
                throw error;
            }
            res.status(200).json({ success: true, contacts: data.data })
        })
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error)
    }
}

