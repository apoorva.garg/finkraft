const request = require('request')


const getContacts = (callback) => {
    const id = '649249007'
    const token = 'db36e02a50b57e081efe533a8a0f834b'
    const url = 'https://books.zoho.com/api/v3/contacts?organization_id=' + id
    const options = {
        url: url,
        headers: {
            'Authorization': `Zoho-authtoken ${token}`
        },
        json: true
    }
    request(options, (error, { body }) => {
        if (error) {
            return callback('Something Went Wrong', undefined)
        }
        else {
            callback(undefined, {
                success: true,
                data: body.contacts
            })
        }
    })
}

module.exports = getContacts