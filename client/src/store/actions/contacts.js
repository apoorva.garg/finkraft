import * as actionTypes from './actionTypes';
import axios from '../../axios-contacts';

export const getContactsStart = () => {
    return {
        type: actionTypes.GET_CONTACTS_START
    }
}

export const getContactsSuccess = (contacts) => {
    return {
        type: actionTypes.GET_CONTACTS_SUCCESS,
        contacts: contacts
    }
}

export const getContactsFail = (error) => {
    return {
        type: actionTypes.GET_CONTACTS_FAIL,
        error: error
    }
}


export const getContacts = () => {
    return dispatch => {
        dispatch(getContactsStart());
        axios.get('/contacts')
            .then(res => {
                const contacts = res.data.contacts;
                dispatch(getContactsSuccess(contacts))
            })
            .catch(err => {
                dispatch(getContactsFail(err));
            })
    }
}

