import * as actionTypes from '../actions/actionTypes';

const initialState = {
    contacts: [],
    loading: false
}

const getContactsStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const getContactsSuccess = (state, action) => {
    const contacts = [...action.contacts]
    const updatedState = {
        contacts: contacts,
        loading: false
    }
    return {
        ...state,
        ...updatedState
    }
}

const getContactsFail = (state, action) => {
    return {
        ...state,
        loading: false
    }
}



const contacts = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_CONTACTS_START: return getContactsStart(state, action)
        case actionTypes.GET_CONTACTS_SUCCESS: return getContactsSuccess(state, action)
        case actionTypes.GET_CONTACTS_FAIL: return getContactsFail(state, action)
        default:
            return state;
    }
}

export default contacts