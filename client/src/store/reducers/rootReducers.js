import {combineReducers} from 'redux'

import ContactsReducer from './contacts'

const rootReducer = combineReducers({
    ContactsReducer
})

export default rootReducer