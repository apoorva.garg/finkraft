import React from 'react';

import './MainNavigation.css';

const mainNavigation = (props) => {
    return (
        <header className="main-navigation">
            <div className="main-navigation-title">
                <h1>Contacts</h1>
            </div>
        </header>
    )
}

export default mainNavigation