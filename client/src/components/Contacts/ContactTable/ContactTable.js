import React from 'react'
import { connect } from 'react-redux';

import './ContactTable.css';

const contactTable = props => {

    const header_data = ['S.No', 'Name', 'Company Name', 'Email', 'Work Phone', 'GST Treatment', 'Receivables', 'Payables']

    const renderHeader = () => {
        return header_data.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    const renderContacts = () => {
        return props.contacts.map((contact, index) => {
            const { contact_id, contact_name, company_name, email, phone, gst_treatment, outstanding_receivable_amount, outstanding_payable_amount } = contact
            return (
                <tr key={contact_id}>
                    <td>{index + 1}</td>
                    <td>{contact_name}</td>
                    <td>{company_name}</td>
                    <td>{email}</td>
                    <td>{phone}</td>
                    <td>{gst_treatment}</td>
                    <td>$ {outstanding_receivable_amount}</td>
                    <td>$ {outstanding_payable_amount}</td>
                </tr>
            )
        })
    }

    return (
        <div>
            <table id="contacts">
                <tbody>
                    <tr>{renderHeader()}</tr>
                    {renderContacts()}
                </tbody>
            </table>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        contacts: state.ContactsReducer.contacts
    }
}

export default connect(mapStateToProps)(contactTable)
