import React from 'react'
import { connect } from 'react-redux';

import Spinner from '../Spinner/Spinner';
import ContactTable from '../Contacts/ContactTable/ContactTable';

const Contacts = props => {

    let showData = <Spinner />

    if (!props.loading) {
        showData = (
            <React.Fragment>
                <h3 style={{ textAlign: 'center' }}>Contacts Data</h3>
                <ContactTable />
            </React.Fragment>
        )
    }

    return (
        <div>
            {showData}
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        contacts: state.ContactsReducer.contacts,
        loading: state.ContactsReducer.loading
    }
}

export default connect(mapStateToProps)(Contacts)
