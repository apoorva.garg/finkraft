import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, Switch } from 'react-router-dom'
import './App.css';

import * as actions from './store/actions/contacts';

import MainNavigation from './components/Navigation/MainNavigation';
import ContactsComponent from './components/Contacts/Contacts'

const App = props => {

  useEffect(()=> {
    props.fetchContacts();
  }, [])

  return (
    <React.Fragment>
      <MainNavigation />
      <main className="main-content">
        <Switch>
          <Redirect from="/" to="/contacts" exact />
          <Route path="/contacts" component={ContactsComponent} />
        </Switch>
      </main>
    </React.Fragment>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    fetchContacts: () => dispatch(actions.getContacts())
  }
}

export default connect(null, mapDispatchToProps)(App);